﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EnumsGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
//TODO: добавить создание папки для добавление списков тоже
            //OptimizationGoal.txt
            string fileName = "CustomEventType.txt";
            var devPath = "../../../";
            List<DataFile> flies = new List<DataFile>();

            var directoryPath = devPath + "Lists";
            var resultPath = devPath + "Result";

            if (!Directory.Exists(directoryPath))
            {
                directoryPath = "Lists";
                resultPath = "Result";

                if (!Directory.Exists(resultPath))
                {
                    Directory.CreateDirectory(resultPath);
                }
            }
               

            //Загружам все файлы
            var files = Directory.GetFiles(directoryPath);

            foreach (var n in files)
            {
                flies.Add(new DataFile{ FileName = Path.GetFileName(n), FileData = File.ReadAllLines(n) });
            }

          
            //Идем по файлам
            foreach (var file in flies)
            {
                var cleared = file.FileData.Select(n => n
                    .Replace(",", "")
                    .Replace("\r\n", "")
                    .Replace("\n", "")
                    .Replace("  ", "")
                    .Replace("    ", ""));

                var tempStucture = $"public enum {file.FileName.Replace(".txt", "")}\n{{\n";
                int counter = 0;
                foreach (var line in cleared)
                {
                    tempStucture += $"\t{line.Trim().Replace(" ","_")} = {counter}";
                    if (counter != cleared.Count() - 1)
                        tempStucture += ",\n";
                    else
                        tempStucture += "\n";
                    counter++;
                }

                tempStucture += "}";

                File.WriteAllText(resultPath + "/" + file.FileName.Replace(".txt","") + "_res.txt",
                    tempStucture);
            }



        }


    }
}
