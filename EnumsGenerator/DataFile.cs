﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EnumsGenerator
{
    public class DataFile
    {
        public string FileName { get; set; }

        public string[] FileData { get; set; }
    }
}
